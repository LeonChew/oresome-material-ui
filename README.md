# oresome-material-ui

[![Travis][build-badge]][build]
[![npm package][npm-badge]][npm]
[![Coveralls][coveralls-badge]][coveralls]

[Click here for demo] (https://leonchew.gitlab.io/oresome-material-ui/)

[Material-UI web site] (https://material-ui.com/)


[build-badge]: https://img.shields.io/travis/user/repo/master.png?style=flat-square
[build]: https://travis-ci.org/user/repo

[npm-badge]: https://img.shields.io/npm/v/npm-package.png?style=flat-square
[npm]: https://www.npmjs.com/package/oresome-material-ui

[coveralls-badge]: https://img.shields.io/coveralls/user/repo/master.png?style=flat-square
[coveralls]: https://coveralls.io/github/user/repo

## Installation
```
// with npm
npm install @material-ui/core @material-ui/icons oresome-material-ui

// with yarn
yarn add @material-ui/core @material-ui/icons oresome-material-ui
```

## Usage
### index.js
```jsx
import React, {Component} from 'react';
import {render} from 'react-dom';

import {BrowserRouter as Router} from 'react-router-dom';
import App from './App';

const RootApp = (
  <Router>
    <App />
  </Router>
);

render(RootApp, document.querySelector('#app'))

```


### App.js
```jsx
import React, { useState } from 'react'
import { MuiThemeProvider, CssBaseline, createMuiTheme, Divider } from '@material-ui/core';
import Routes, { navigationRoutes } from './Routes';
import { Provider as AppProvider, defaultContext } from './AppProvider';

import { MainLayout, MenuItems, TopBarActions, Colours } from '../../src';
import SettingsComponentContent from './components/SettingsComponentContent';

import './App.css';

const Logo = props => (
  <div>Logo</div>
);

const App = (props) => {
  const [state, setState] = useState(defaultContext);

  const handleConfigChange = (name, value) => {
    setState({
      ...state,
      [name]: value
    });
  }

  const { colour, type, allowNightMode, allowThemeColour, allowSideBar } = state;

  const theme = createMuiTheme({
    palette: { ...Colours[colour], type: type },
    typography: {
      useNextVariants: true
    }
  });

  return (
    <AppProvider
      value={{
        ...state,
        handleConfigChange: handleConfigChange
      }}>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <MainLayout
          menuItems={(p) => <MenuItems navigationRoutes={navigationRoutes} {...p} />}
          topBarActions={TopBarActions}
          logo={Logo}
          title="App Title"
          updateTheme={handleConfigChange}
          colour={colour}
          nightModeDisabled={!allowNightMode}
          themeColourDisabled={!allowThemeColour}
          sideBarDisabled={!allowSideBar}
          settingsPanelContentStart={<div>Content at start of panel <Divider style={{ marginTop: '10px', marginBottom: '10px' }} /></div>}
          settingsPanelContentEnd={(
            <SettingsComponentContent
              onToggleThemeColour={() => handleConfigChange('allowThemeColour', !allowThemeColour)}
              onToggleAllowNightMode={() => handleConfigChange('allowNightMode', !allowNightMode)}
              onToggleSideBar={() => handleConfigChange('allowSideBar', !allowSideBar)}
            />
          )}
        >
          <Routes />
        </MainLayout>
      </MuiThemeProvider>
    </AppProvider>
  )
};

export default App;

      
```

### AppProvider.js
```jsx
import { createContext } from 'react';

export const defaultContext = {
  colour: 'orange',
  type: 'light',
  handleConfigChange: () => { },
  allowNightMode: true,
  allowThemeColour: true,
  allowSideBar: true
}

export const { Provider, Consumer } = createContext(defaultContext);

```

### Routes.js
```jsx
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Dashboard, Assignment, List } from '@material-ui/icons';

import DashboardPage from './containers/Dashboard';
import RegisterPage from './containers/Register';
import TasksPage from './containers/Tasks';

export const navigationRoutes = [
  {
    exact: true,
    path: '/',
    name: 'Dashboard',
    icon: Dashboard,
    component: DashboardPage
  },
  {
    path: '/register',
    name: 'Register',
    description: 'Register',
    icon: List,
    component: RegisterPage
  },
  {
    path: '/tasks/:tabId?',
    name: 'Tasks',
    description: 'Task Description',
    icon: Assignment,
    component: TasksPage
  }
];


export default () => (
  <Switch>
    {navigationRoutes.map((r, i) => <Route key={i} exact={r.exact} path={r.path} component={r.component} />)}
  </Switch>
);
```