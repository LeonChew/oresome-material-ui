import axios from 'axios';

const client = axios.create({
  
  baseURL : 'http://localhost:5000/api'
});

const request = (options) => {
  const onSuccess = (response) => {
    return response;
  };

  const onError = (error) => {
    if(error.response){
      console.error('Status: ', error.response.status);
      console.error('Data: ', error.response.data);
      console.error('Header: ', error.response.headers);
    } else {
      console.error('Error Message: ', error.message);
    }

    return Promise.reject(error.response || error.message);
  }
  return client(options).then(onSuccess).catch(onError);
};

export default request;
