import React from 'react';
import { List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import SwapHorizIcon from "@material-ui/icons/SwapHoriz";

const SettingsComponentContent = ({ onToggleAllowNightMode, onToggleThemeColour, onToggleSideBar }) => {
  return (
    <List>
      <ListItem button onClick={() => { if (onToggleAllowNightMode) onToggleAllowNightMode() }}>
        <ListItemIcon><SwapHorizIcon /></ListItemIcon>
        <ListItemText primary="Toggle Allow Night" />
      </ListItem>
      <ListItem button onClick={() => { if (onToggleThemeColour) onToggleThemeColour() }}>
        <ListItemIcon><SwapHorizIcon /></ListItemIcon>
        <ListItemText primary="Toggle Allow Theme Colour" />
      </ListItem>
      <ListItem button onClick={() => { if (onToggleSideBar) onToggleSideBar() }}>
        <ListItemIcon><SwapHorizIcon /></ListItemIcon>
        <ListItemText primary="Toggle Allow Sidebar" />
      </ListItem>
    </List>
  );
}

export default SettingsComponentContent;