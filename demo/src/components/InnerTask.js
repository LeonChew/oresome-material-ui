import React, { Component } from 'react';
import { Typography } from '@material-ui/core';

const InnerTask = (props) => {
  return (
    <Typography variant="h6">Inner Task</Typography>
  );
}

export default InnerTask;