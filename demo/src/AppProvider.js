import { createContext } from 'react';

export const defaultContext = {
  colour: 'orange',
  type: 'light',
  handleConfigChange: () => { },
  allowNightMode: true,
  allowThemeColour: true,
  allowSideBar: true
}

export const { Provider, Consumer } = createContext(defaultContext);