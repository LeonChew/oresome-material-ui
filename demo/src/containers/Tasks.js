import React, { useState } from 'react';
import { Grid, Tabs, Tab, Typography } from "@material-ui/core";
import InnerTask from '../components/InnerTask';
import { Route } from "react-router";

const Tasks = (props) => {

  const tabPages = [
    { index: 0, label: "Task 1", href: "/1", component: () => <InnerTask name={'Task 1'} /> },
    { index: 1, label: "Task 2", href: "/2", component: InnerTask },
    { index: 2, label: "Task 3", href: "/3", component: InnerTask },
    { index: 3, label: "Task 4", href: "/4", component: InnerTask },
  ];

  const [currentTab, setCurrentTab] = useState(0);

  const changeTab = (index) => {
    setCurrentTab(index);
    const newPage = tabPages[index];
    props.history.push(`${props.match.path.replace('/:tabId?', newPage.href)}`);
  }

  return (
    <div>
      <Typography variant="h6">Tasks</Typography>
      <Grid item xs={12} xl={8}>
        <Tabs
          value={currentTab}
          onChange={(e, v) => changeTab(v)}
          indicatorColor="primary"
          textColor="primary"
        >
          {tabPages.map((p) => (
            <Tab key={p.index} label={p.label} selected={currentTab === p.index} />
          ))}
        </Tabs>
        <Grid item xs={12} xl={8}>
          {tabPages.map((p, i) => (
            <Route key={i} exact path={`${props.match.path}${p.href}`} component={p.component} />
          ))}
        </Grid>
      </Grid>
    </div>
  );
}

export default Tasks;