import React from 'react';
import { Typography, Grid, makeStyles, Paper } from '@material-ui/core';
import ReactPlaceholder from 'react-placeholder';
import "react-placeholder/lib/reactPlaceholder.css";

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary
  }
}));

const Dashboard = (props) => {

  const classes = useStyles();
  return (
    <div className={'page ' + classes.root}>
      <Typography className="page-title" variant="h6">Dashboard</Typography>
      <Grid container spacing={5}>
        <Grid item xs={12} sm={6} md={3} lg={3}>
          <Paper className={classes.paper}>
            <ReactPlaceholder type='media' rows={5} ready={false}>
              <div></div>
            </ReactPlaceholder>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} md={3} lg={3}>
          <Paper className={classes.paper}>
            <ReactPlaceholder type='media' rows={5} ready={false}>
              <div></div>
            </ReactPlaceholder>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} md={3} lg={3}>
          <Paper className={classes.paper}>
            <ReactPlaceholder type='media' rows={5} ready={false}>
              <div></div>
            </ReactPlaceholder>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6} md={4} lg={3}>
          <Paper className={classes.paper}>
            <ReactPlaceholder type='media' rows={5} ready={false}>
              <div></div>
            </ReactPlaceholder>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Paper className={classes.paper}>
            <ReactPlaceholder type='media' rows={5} ready={false}>
              <div></div>
            </ReactPlaceholder>
          </Paper>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Paper className={classes.paper}>
            <ReactPlaceholder type='media' rows={5} ready={false}>
              <div></div>
            </ReactPlaceholder>
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <ReactPlaceholder type='text' rows={8} ready={false}>
              <div></div>
            </ReactPlaceholder>
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <ReactPlaceholder type='text' rows={8} ready={false}>
              <div></div>
            </ReactPlaceholder>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
}

export default Dashboard;