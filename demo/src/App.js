import React, { useState } from 'react'
import { MuiThemeProvider, CssBaseline, createMuiTheme, Divider } from '@material-ui/core';
import Routes, { navigationRoutes } from './Routes';
import { Provider as AppProvider, defaultContext } from './AppProvider';

import { MainLayout, MenuItems, TopBarActions, Colours } from '../../src';
import SettingsComponentContent from './components/SettingsComponentContent';

import './App.css';

const Logo = props => (
  <div>Logo</div>
);

const App = (props) => {
  const [state, setState] = useState(defaultContext);

  const handleConfigChange = (name, value) => {
    setState({
      ...state,
      [name]: value
    });
  }

  const { colour, type, allowNightMode, allowThemeColour, allowSideBar } = state;

  const theme = createMuiTheme({
    palette: { ...Colours[colour], type: type },
    typography: {
      useNextVariants: true
    }
  });

  return (
    <AppProvider
      value={{
        ...state,
        handleConfigChange: handleConfigChange
      }}>
      <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <MainLayout
          menuItems={(p) => <MenuItems navigationRoutes={navigationRoutes} {...p} />}
          topBarActions={TopBarActions}
          logo={Logo}
          title="App Title"
          updateTheme={handleConfigChange}
          colour={colour}
          theme={theme}
          nightModeDisabled={!allowNightMode}
          themeColourDisabled={!allowThemeColour}
          sideBarDisabled={!allowSideBar}
          settingsPanelContentStart={<div>Content at start of panel <Divider style={{ marginTop: '10px', marginBottom: '10px' }} /></div>}
          settingsPanelContentEnd={(
            <SettingsComponentContent
              onToggleThemeColour={() => handleConfigChange('allowThemeColour', !allowThemeColour)}
              onToggleAllowNightMode={() => handleConfigChange('allowNightMode', !allowNightMode)}
              onToggleSideBar={() => handleConfigChange('allowSideBar', !allowSideBar)}
            />
          )}
        >
          <Routes />
        </MainLayout>
      </MuiThemeProvider>
    </AppProvider>
  )
};

export default App;
