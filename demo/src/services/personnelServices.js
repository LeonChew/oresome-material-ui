import request from '../common/request';


const URL = '/personnels'
export const GetPersonnels = params => {
  return request({
    url: URL,
    method: 'GET',
    params
  });
};
