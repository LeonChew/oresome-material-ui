import React, {Component} from 'react';
import {render} from 'react-dom';

import {BrowserRouter as Router} from 'react-router-dom';
import App from './App';
import "@babel/polyfill";

const RootApp = (
  <Router>
    <App />
  </Router>
);

render(RootApp, document.querySelector('#demo'))
