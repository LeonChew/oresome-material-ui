export const FieldArrayToJson = array => {
  let obj = {};

  for(let f of array){
    obj = {...obj, ...f}
  }

  return obj;
}