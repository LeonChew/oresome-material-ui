import React from 'react';
import { Drawer, Typography, Divider, IconButton, Switch, makeStyles } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import ThemeColours from '../ThemeSelect/ThemeColours';
import { Consumer as LayoutConsumer } from '../Layout/LayoutContext';

const useStyles = makeStyles(theme => ({
  paper: {
    minWidth: '250px'
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    padding: '0 16px',
    ...theme.mixins.toolbar
  },
  titleContainer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  settingItem: {
    marginBottom: '16px'
  },
  content: {
    padding: '16px',
    display: 'flex',
    flexGrow: 1,
    flexDirection: 'column'
  },
  switchWrapper: {
    display: 'flex',
    flexDirection: 'row',
    '& .label': {
      flex: 1,
      display: 'flex',
      alignItems: 'center'
    }
  },
  drawerClose: {
    padding: '5px'
  }
}));



const SettingPanel = (props) => {

  const toggleNightMode = (event) => {
    props.layoutContext.updateTheme('type', event.target.value === 'dark' ? 'light' : 'dark');
  }

  const theme = props.theme;
  let classes = useStyles();
  if (props.classes) classes = { ...classes, ...props.classes };

  return (
    <Drawer
      variant={props.variant}
      anchor='right'
      open={props.open}
      onClose={props.onClose}
      classes={{ paper: classes.paper }}
      ModalProps={{
        keepMounted: true, // Better open performance on mobile.
      }}
    >
      <div className={classes.toolbar}>
        <div className={classes.titleContainer}>
          <Typography variant="subtitle1">Application Settings</Typography>
          <IconButton size='small' className={classes.drawerClose} onClick={() => { if (props.onClose) props.onClose(); }}><CloseIcon fontSize='small' /></IconButton>
        </div>
      </div>
      <Divider />
      <div className={classes.content}>
        {props.contentStart}
        {!props.themeColourDisabled && (
          <div className={classes.settingItem}>
            <div><Typography variant="caption">Theme Colour</Typography></div>
            <ThemeColours />
          </div>
        )}
        {!props.nightModeDisabled &&
          <div className={classes.switchWrapper}>
            <div className={'label'}><Typography variant="caption">Night Mode</Typography></div>
            <Switch
              checked={theme.palette.type === 'dark' ? true : false}
              value={theme.palette.type}
              onChange={toggleNightMode}
            ></Switch>
          </div>
        }
        <div>
          {props.children}
        </div>
      </div>
    </Drawer>
  );
};

const SettingPanelComponent = props => (
  <LayoutConsumer>
    {layoutContext => <SettingPanel {...props} layoutContext={layoutContext} />}
  </LayoutConsumer>
);

export default SettingPanelComponent;