import React, { Component, useState } from 'react';
import PropTypes from 'prop-types';
import { AppBar, Toolbar, IconButton, Typography, Hidden, makeStyles, createMuiTheme } from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import SideBar from './SideBar';
import classNames from 'classnames';
import SettingPanel from '../Setting/SettingPanel';
import { Provider } from './LayoutContext';

const drawerWidth = '250px';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    height: '100vh',
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: '100%',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    [theme.breakpoints.up('md')]: {
      width: `calc(100% - ${drawerWidth})`,
    },
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    })
  },
  navIconHide: {
    [theme.breakpoints.up('md')]: {
      display: 'none',
    }
  },
  appToolBar: {
    paddingLeft: theme.spacing(3),
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar
  },
  title: {
    flex: 1
  },
  hide: {
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  menuButton: {
    marginLeft: 12,
    marginRight: '36px'
  },
  drawerPaper: {
    width: drawerWidth,
    height: '100vh',
    backgroundColor: '#252525',
    color: 'white',
    whiteSpace: 'nowrap',
    [theme.breakpoints.up('md')]: {
      position: 'relative',
    },
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    })
  },
  drawerPaperClose: {
    overflowX: 'hidden',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(9),
    },
    '& .menu-items': {
      '& .menu-item': {
        padding: '10px',
        marginRight: '15px',
        '& .menu-item-text': {
          visibility: 'hidden'
        }
      },
      '& .selected .menu-item': {
        backgroundColor: 'transparent',
        boxShadow: 'none',
        '& .menu-icon': {
          color: theme.palette.primary.main
        }
      }
    }
  },
  logo: {
    flexGrow: 1,
    lineHeight: 0,
    marginLeft: '24px',
    color: 'white'
  },
  toolbarLabel: {
    color: "#fff",
  },
  contentWrapper: {
    flexGrow: 1,
    overflow: 'auto',
    display: 'flex',
    flexDirection: 'column',
    backgroundColor: theme.palette.background.default
  },
  content: {
    overflow: 'auto',
    padding: theme.spacing(3),
  },
  topBarActions: {
    marginRight: '12px'
  },
  flexDiv: {
    display: 'flex'
  }
}));

const MainLayout = (props) => {

  const { theme, updateTheme, themeColourDisabled, sideBarDisabled, colour, title, menuItems, children, nightModeDisabled, settingsPanelContentStart, settingsPanelContentEnd } = props;

  const Logo = props.logo;
  const TopBarActions = props.topBarActions;

  const [sideBarState, setSideBarState] = useState({
    mainOpen: props.initialSideBarOpen === undefined ? true : props.initialSideBarOpen,
    mobileOpen: false
  });

  const [settingPanelOpen, setSettingPanelOpen] = useState(false);

  const toggleLeftSideBar = () => {
    setSideBarState({
      mainOpen: !sideBarState.mainOpen,
      mobileOpen: !sideBarState.mobileOpen
    });
  }

  const toggleSettingPanel = () => {
    setSettingPanelOpen(!settingPanelOpen);
  }


  let classes = useStyles();

  if (props.classes) classes = { ...classes, ...props.classes };


  return (
    <Provider
      value={{
        updateTheme: updateTheme,
        colour: colour
      }}
    >
      <div className={classes.root}>
        <AppBar className={classNames(classes.appBar, { [classes.appBarShift]: sideBarState.mainOpen && !sideBarDisabled })}>
          <Toolbar
            disableGutters={true}
            className={classNames({ [classes.appToolBar]: sideBarState.mainOpen || sideBarDisabled })}>
            {!sideBarDisabled && (
              <IconButton
                className={classNames(classes.menuButton, { [classes.hideMdUp]: sideBarState.mainOpen }, { [classes.hide]: sideBarDisabled })}
                color="inherit"
                onClick={toggleLeftSideBar}
              >
                <MenuIcon />
              </IconButton>
            )}

            <Typography
              className={classes.title}
              variant="h6"
              color="inherit">{title}</Typography>
            <div className={classes.topBarActions}>
              <TopBarActions toggleSettings={toggleSettingPanel} />
            </div>

          </Toolbar>
        </AppBar>
        {!sideBarDisabled && (
          <React.Fragment>
            <Hidden mdUp>
              <SideBar
                closeOnMenuItemClick
                theme={theme}
                classes={classes}
                drawerClasses={{
                  paper: classes.drawerPaper,
                }}
                open={sideBarState.mobileOpen}
                onClose={toggleLeftSideBar}
                logo={<Logo />}
                menuItems={menuItems}
              />
            </Hidden>
            <Hidden smDown implementation="css">
              <SideBar
                variant="permanent"
                open={sideBarState.mainOpen}
                onClose={toggleLeftSideBar}
                theme={theme}
                classes={classes}
                drawerClasses={{
                  paper: classNames(classes.drawerPaper, !sideBarState.mainOpen && classes.drawerPaperClose),
                }}
                logo={<Logo />}
                menuItems={menuItems}
              />
            </Hidden>
          </React.Fragment>
        )}

        <div className={classes.contentWrapper}>
          <div className={classes.toolbar} />
          <div className={classes.content}>
            {children}
          </div>
        </div>
        <SettingPanel
          theme={theme}
          variant="temporary"
          open={settingPanelOpen}
          onClose={() => setSettingPanelOpen(false)}
          nightModeDisabled={nightModeDisabled}
          themeColourDisabled={themeColourDisabled}
          contentStart={settingsPanelContentStart}
        >
          {settingsPanelContentEnd}
        </SettingPanel>
      </div>
    </Provider>
  );
}

MainLayout.propTypes = {
  themeColourDisabled: PropTypes.bool,
  logo: PropTypes.func,
  topBarActions: PropTypes.func,
  classes: PropTypes.object,
  updateTheme: PropTypes.func,
  colour: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  menuItems: PropTypes.func.isRequired,
  children: PropTypes.any,
  nightModeDisabled: PropTypes.bool,
  settingsPanelContentStart: PropTypes.any,
  settingsPanelContentEnd: PropTypes.any,
  theme: PropTypes.any.isRequired
};

MainLayout.defaultProps = {
  colour: 'orange',
};

export default MainLayout;