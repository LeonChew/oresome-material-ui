import React from 'react';
import { makeStyles } from '@material-ui/core';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ToolTip from '@material-ui/core/Tooltip';
import List from '@material-ui/core/List';
import Zoom from '@material-ui/core/Zoom';
import CustomNavLink from './CustomNavLink';

const useStyles = makeStyles(theme => ({
  icon: {
    color: 'white'
  }
}));

const MenuItems = props => {

  const classes = useStyles();

  return (
    <List>
      {props.navigationRoutes.map((r, i) => {
        return (
          <CustomNavLink
            path={r.path}
            key={i}
            onClick={() => { if (props.closeOnClick && props.onClose) props.onClose(); }}
          >
            <ToolTip
              title={r.description ? r.description : ''}
              placement='right'
              TransitionComponent={Zoom}
              enterDelay={800}
            >
              <ListItem>
                <ListItemIcon className={classes.icon}><r.icon /></ListItemIcon>
                <ListItemText disableTypography={true} primary={r.name}></ListItemText>
              </ListItem>
            </ToolTip>
          </CustomNavLink>
        );
      })}
    </List>
  );
}

export default MenuItems;