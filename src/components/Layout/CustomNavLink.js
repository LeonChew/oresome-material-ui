import React from 'react';
import { NavLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core';
import useReactRouter from 'use-react-router';

const useStyles = makeStyles(theme => ({
  icon: {
    color: 'white'
  },
  link: {
    color: 'white',
    textDecoration: 'none',
    '&.selected': {
      color: theme.palette.secondary['500'],
      fontWeight: 'bold'
    },
    '&.selected svg': {
      color: theme.palette.secondary['500']
    }
  },

}));

const trimParameters = (route) => {
  let components = route.split('/');
  let trimmedComponents = components.filter(c => c.indexOf(':') != 0);
  return trimmedComponents.join('/');
}

const CustomNavLink = ({ path, classes, children, onClick }) => {

  let compiledClasses = useStyles();
  const routeContext = useReactRouter();
  if (classes) compiledClasses = { ...compiledClasses, ...classes };

  const isMenuSelected = () => {
    let currentTrimmedPathname = trimParameters(routeContext.location.pathname.toLowerCase());
    let trimmedRoutePath = trimParameters(path.toLowerCase());
    return (currentTrimmedPathname == trimmedRoutePath) ||
      (currentTrimmedPathname.indexOf(trimmedRoutePath) == 0 && trimmedRoutePath != '/');
  }

  return (
    <NavLink
      className={`${compiledClasses.link} ${isMenuSelected() ? 'selected' : ''}`}
      to={trimParameters(path)}
      exact
      activeClassName="selected"
      onClick={onClick}
    >
      {children}
    </NavLink>
  );
}

export default CustomNavLink;