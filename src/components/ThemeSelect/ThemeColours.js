import React from 'react';
import { makeStyles } from '@material-ui/core';
import Colours from './Colours';
import { Consumer as LayoutConsumer } from '../Layout/LayoutContext';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    width: '150px',
    marginTop: '6px',
    justifyContent: 'space-between'
  },
  square: {
    width: '16px',
    height: '16px',
    cursor: 'pointer'
  },
  orange: {
    backgroundColor: Colours.orange.primary['500'],
    '&:hover': {
      outline: `3px solid ${Colours.orange.secondary['300']}`
    },
    '&.selected': {
      outline: `3px solid ${Colours.orange.secondary['300']}`
    }
  },
  blue: {
    backgroundColor: Colours.blue.primary['500'],
    '&:hover': {
      outline: `3px solid ${Colours.blue.secondary['300']}`
    },
    '&.selected': {
      outline: `3px solid ${Colours.blue.secondary['300']}`
    }
  },
  green: {
    backgroundColor: Colours.green.primary['500'],
    '&:hover': {
      outline: `3px solid ${Colours.green.secondary['300']}`
    },
    '&.selected': {
      outline: `3px solid ${Colours.green.secondary['300']}`
    }
  },
  purple: {
    backgroundColor: Colours.purple.primary['500'],
    '&:hover': {
      outline: `3px solid ${Colours.purple.secondary['300']}`
    },
    '&.selected': {
      outline: `3px solid ${Colours.purple.secondary['300']}`
    }
  },
  turquoise: {
    backgroundColor: Colours.turquoise.primary['500'],
    '&:hover': {
      outline: `3px solid ${Colours.turquoise.secondary['300']}`
    },
    '&.selected': {
      outline: `3px solid ${Colours.turquoise.secondary['300']}`
    }
  }
}));

const ThemeColours = (props) => {

  const handleClick = (event) => {
    props.layoutContext.updateTheme('colour', event.currentTarget.title);
  }

  const isSelected = (colour) => {
    return colour === props.layoutContext.colour ? 'selected' : '';
  }

  let classes = useStyles();
  if (props.classes) classes = { ...classes, ...props.classes };

  return (
    <div className={classes.root}>
      <div
        className={`${classes.square} ${classes.orange} ${isSelected('orange')}`}
        onClick={handleClick}
        title="orange"
      ></div>
      <div className={`${classes.square} ${classes.blue} ${isSelected('blue')}`}
        onClick={handleClick}
        title="blue"
      ></div>
      <div className={`${classes.square} ${classes.green} ${isSelected('green')}`}
        onClick={handleClick}
        title="green"
      ></div>
      <div className={`${classes.square} ${classes.purple} ${isSelected('purple')}`}
        onClick={handleClick}
        title="purple"
      ></div>
      <div className={`${classes.square} ${classes.turquoise} ${isSelected('turquoise')}`}
        onClick={handleClick}
        title="turquoise"
      ></div>
    </div>
  );
}

const ThemeColoursComponent = props => (
  <LayoutConsumer>
    {layoutContext => <ThemeColours {...props} layoutContext={layoutContext} />}
  </LayoutConsumer>
);

export default ThemeColoursComponent;